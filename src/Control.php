<?php

namespace Duna\Forms;

use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\UI\Control as BaseControl;
use Nette\DI\MissingServiceException;
use Nette\InvalidStateException;

/**
 * @package Duna\Forms
 * @property-write callable[] $callback
 */
abstract class Control extends BaseControl
{

    /** @var \Doctrine\ORM\EntityManagerInterface */
    protected $em;
    private $specialCallback = [];

    public function __construct(BaseControl $parent, $name, EntityManagerInterface $em)
    {
        parent::__construct($parent, $name);
        $this->em = $em;
    }

    /**
     * @param callable $callback
     */
    public function setCallback(callable $callback)
    {
        $this->addCallback('simple', $callback);
    }

    public function addCallback($name, callable $callback)
    {
        if (!array_key_exists($name, $this->specialCallback))
            $this->specialCallback[$name] = [];

        $this->specialCallback[$name][] = $callback;
    }

    public function getCallback()
    {
        return $this->getSpecialCallback('simple');
    }

    public function getSpecialCallback($name)
    {
        return array_key_exists($name, $this->specialCallback) ? $this->specialCallback[$name] : [];
    }


    public function isAllowed($operation)
    {
        return $this->getPresenter()->getUser()->isAllowed($this->getPresenter()->name, $operation);
    }

    public function isAllowedOperation($operation, $name)
    {
        try {
            /** @var \Duna\Plugin\SecurityComponent\Authorizator $authorizator */
            $authorizator = $this->getPresenter(true)->context->getService('authorizatorComponent');

            $identity = $this->getPresenter(true)->getUser()->getIdentity();
            if (!$identity)
                throw new InvalidStateException();
            $email = $identity->getData()['email'];
            if ($authorizator->isAllowed($email, '_' . $this->getPresenter()->getName() . '_' . $name, $operation)) {
                return true;
            }
        } catch (MissingServiceException $e) {
            //TODO dodělat opvěření pomocí výchozích oprávnění
        } catch (InvalidStateException $e) {
        }
        return false;
    }
}