<?php

namespace Duna\Forms;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Forms
 */
class Form extends \Duna\UI\Form\Form
{
    protected $configuration = [
        'labels'   => [],
        'required' => [],
    ];
    private $success = [];

}